//
//  PNVSelectCountry.h
//  PhoneNumberValidate
//
//  Created by Minh Nguyen on 6/17/14.
//  Copyright (c) 2014 none. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PNVSelectCountry : UIView

@property(nonatomic, copy) void(^callback)(NSString* countryCode, NSString* countryName);

+(PNVSelectCountry*)setupInView:(UIView*)view;

-(void)toggleInView:(UIView*)view;

@end
