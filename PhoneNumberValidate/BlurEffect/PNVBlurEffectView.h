//
//  PNVBlurEffectView.h
//  PhoneNumberValidate
//
//  Created by Minh Nguyen on 6/17/14.
//  Copyright (c) 2014 none. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PNVBlurEffectView : UIView

@property (nonatomic, strong) UIView *viewNeedToCapture;
@property (nonatomic, assign) NSUInteger iterations;
@property (nonatomic, assign) CGFloat blurRadius;
@property (nonatomic, strong) UIColor *tintColor;

- (void)showWithDuration:(CGFloat)duration iterations:(NSUInteger)iter blurRadius:(CGFloat)radius tintColor:(UIColor *)color;
- (void)hideWithDuration:(CGFloat)duration;
- (void)updateBlurView;

@end
