//
//  PNVBlurEffectView.m
//  PhoneNumberValidate
//
//  Created by Minh Nguyen on 6/17/14.
//  Copyright (c) 2014 none. All rights reserved.
//

#import "PNVBlurEffectView.h"
#import "UIImage+Blur.h"

@implementation PNVBlurEffectView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.clipsToBounds = YES;
        self.alpha = 0;
        self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}


- (UIImage *)snapshotOfSuperview:(UIView *)superview
{
    CGFloat scale = (self.iterations > 0)? 4.0f / MAX(8, floor(self.blurRadius)) : 1.0f;
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, -self.frame.origin.x, -self.frame.origin.y);
    [superview.layer renderInContext:context];
    UIImage *snapshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshot;
}

- (void)showWithDuration:(CGFloat)duration iterations:(NSUInteger)iter blurRadius:(CGFloat)radius tintColor:(UIColor *)color
{
    self.iterations = iter;
    self.blurRadius = radius;
    self.tintColor = color;
    
    UIImage *snapshot = [self snapshotOfSuperview:_viewNeedToCapture];
    NSUInteger iterations = MAX(0, (NSInteger)iter - 1);
    UIImage *blurredImage = [snapshot blurredImageWithRadius:radius
                                                  iterations:iterations
                                                   tintColor:color];
    self.layer.contents = (id)blurredImage.CGImage;
    self.layer.contentsScale = blurredImage.scale;
    
    self.alpha = 0.0;
    
    [UIView animateWithDuration:duration animations:^{
        self.alpha = 1.0;
    }];
}

- (void)hideWithDuration:(CGFloat)duration
{
    [UIView animateWithDuration:duration animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
}

- (void)updateBlurView
{
    
    UIImage *snapshot = [self snapshotOfSuperview:_viewNeedToCapture];
    NSUInteger iterations = MAX(0, (NSInteger)self.iterations - 1);
    UIImage *blurredImage = [snapshot blurredImageWithRadius:self.blurRadius
                                                  iterations:iterations
                                                   tintColor:self.tintColor];
    self.layer.contents = (id)blurredImage.CGImage;
    self.layer.contentsScale = blurredImage.scale;
}

- (void)dealloc
{
    self.viewNeedToCapture = nil;
}

@end
