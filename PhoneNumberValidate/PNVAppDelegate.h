//
//  PNVAppDelegate.h
//  PhoneNumberValidate
//
//  Created by Minh Nguyen on 6/17/14.
//  Copyright (c) 2014 none. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PNVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
