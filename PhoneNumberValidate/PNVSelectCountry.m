//
//  PNVSelectCountry.m
//  PhoneNumberValidate
//
//  Created by Minh Nguyen on 6/17/14.
//  Copyright (c) 2014 none. All rights reserved.
//

#import "PNVSelectCountry.h"
#import "CountryPicker.h"
#import "PNVBlurEffectView.h"

@interface PNVSelectCountry()<CountryPickerDelegate>

@property(nonatomic, weak)IBOutlet UIButton *doneButton;
@property(nonatomic, weak)IBOutlet CountryPicker *picker;
@property(nonatomic, strong)PNVBlurEffectView *blurView;

@end

@implementation PNVSelectCountry

+(PNVSelectCountry *)setupInView:(UIView*)view{
    PNVSelectCountry* this = (PNVSelectCountry *)[[[NSBundle mainBundle]loadNibNamed:@"PNVSelectCountry" owner:self options:nil] objectAtIndex:0];
    
    if (this) {
        
        CGRect frame = this.frame;
        frame.origin = CGPointMake(0, view.frame.size.height);
        frame.size = view.frame.size;
        
        [this setFrame:frame];
    }
    
    return this;
}

#pragma mark - Make view show or hide

-(void)toggleInView:(UIView *)view{
    CGRect frame = self.frame;
    
    if (frame.origin.y > 0) {
        frame.origin = CGPointMake(0, 0);
        
        if (_blurView) {
            [_blurView removeFromSuperview];
            _blurView = nil;
        }
        
        _blurView = [[PNVBlurEffectView alloc]initWithFrame:self.bounds];
        _blurView.viewNeedToCapture = view;
        [_blurView showWithDuration:0.0f iterations:5 blurRadius:10.0f tintColor:[UIColor clearColor]];
        [self insertSubview:_blurView atIndex:0];
    }
    else{
        frame.origin = CGPointMake(0, frame.size.height);
    }
    
    [UIView animateWithDuration:0.2f animations:^{
        [self setFrame:frame];
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - Handler button event

-(IBAction)onTapByDone:(id)sender{
    if (_callback) {
        _callback(_picker.selectedCountryCode, _picker.selectedCountryName);
    }
    
    [self toggleInView:self.superview];
}

#pragma mark - Picker Delegate

-(void)countryPicker:(CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code{
    
}

@end
