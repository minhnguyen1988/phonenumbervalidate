//
//  PNVViewController.m
//  PhoneNumberValidate
//
//  Created by Minh Nguyen on 6/17/14.
//  Copyright (c) 2014 none. All rights reserved.
//

#import "PNVViewController.h"
#import "PNVSelectCountry.h"
#import "NBPhoneNumberUtil.h"
#import "NBPhoneNumber.h"

typedef enum {
    kcNumber,
    kcCountry,
    kcDelete
}ButtonCommand;

@interface PNVViewController ()

@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;

@property(nonatomic, copy) PNVSelectCountry *selectCountryView;

@property(nonatomic, copy)NSString* countryCode;
@property(nonatomic, copy)NSString* numberString;
@property(nonatomic, assign)BOOL isValid;

@end

@implementation PNVViewController

#pragma mark - View's life cycles

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _countryCode = @"AF";
    _numberString = @"";
    
    _numberLabel.adjustsFontSizeToFitWidth = YES;
    
    _selectCountryView = [PNVSelectCountry setupInView:self.view];
    
    [self initKeyPad];
    
    [self.view addSubview:_selectCountryView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Generate Layout

- (void)initKeyPad{
    int rows = 4;
    int cols = 3;
    
    float distanceX = 20.0f;
    float distanceY = 10.0f;
    
    CGSize size = CGSizeMake(75, 75);
    
    CGPoint startOrigin = CGPointMake(_numberLabel.frame.origin.x, _numberLabel.frame.origin.y + _numberLabel.frame.size.height * 1.5);
    
    for (int r = 0; r < rows; r++) {
        
        for (int c = 0; c < cols; c++) {
            
            UIButton *keypad = [UIButton buttonWithType:UIButtonTypeCustom];
            
            float x = startOrigin.x + c*(size.width + distanceX);
            float y = startOrigin.y + r*(size.height + distanceY);
            
            [keypad setFrame:CGRectMake(x, y, size.width, size.height)];
            
            [keypad setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [keypad setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
            
            [keypad addTarget:self action:@selector(onTapByKeyPad:) forControlEvents:UIControlEventTouchUpInside];
            [keypad addTarget:self action:@selector(onPressingKeyPad:) forControlEvents:UIControlEventTouchDown];
            [keypad addTarget:self action:@selector(onPressingKeyPad:) forControlEvents:UIControlEventTouchDragInside];
            [keypad addTarget:self action:@selector(onReleasedKeyPad:) forControlEvents:UIControlEventTouchDragOutside];
            
            keypad.tag = kcNumber;
            
            int number = r*cols + c + 1;
            
            if (number < 10){
                [keypad setTitle:[NSString stringWithFormat:@"%d", number] forState:UIControlStateNormal];
            }
            else{
                switch (number) {
                    case 10:{
                        [keypad setContentMode:UIViewContentModeCenter];
                        [keypad setImage:[UIImage imageNamed:@"AF"] forState:UIControlStateNormal];
                        keypad.tag = kcCountry;
                        
                        __weak PNVViewController *this = self;
                        
                        [_selectCountryView setCallback:^(NSString *countryCode, NSString* countryName) {
                            [keypad setImage:[UIImage imageNamed:countryCode] forState:UIControlStateNormal];
                            [this.countryLabel setText:countryName];
                            this.countryCode = countryCode;
                            this.isValid = NO;
                            [this.statusLabel setText:@""];
                            this.numberString = @"";
                            [this.numberLabel setText:@""];
                            
                        }];
                    }
                        break;
                    case 11:
                        [keypad setTitle:@"0" forState:UIControlStateNormal];
                        break;
                    case 12:{
                        [keypad setTitle:@"⬅︎" forState:UIControlStateNormal];
                        keypad.tag = kcDelete;
                    }
                        break;
                    default:
                        break;
                }
            }
            
            keypad.layer.borderWidth = 1.0f;
            keypad.layer.borderColor = [UIColor colorWithRed:40.0f/255.0f green:70.0f/255.0f blue:131.0f/255.0f alpha:1.0f].CGColor;
            keypad.layer.cornerRadius = keypad.bounds.size.width*0.5;
            keypad.layer.masksToBounds = YES;
            
            [self.view addSubview:keypad];
            
        }
    }
}

#pragma mark - Verify Phone
-(void)verifyPhoneNumber:(NSString*)number withCode:(NSString*)countryCode{
    
    NSError *aError = nil;
    
    NBPhoneNumberUtil *phoneUtil = [NBPhoneNumberUtil sharedInstance];
    
    NBPhoneNumber *myNumber = [phoneUtil parse:number defaultRegion:countryCode error:&aError];
    
    _isValid = [phoneUtil isValidNumber:myNumber];
    
    NBEValidationResult validation;
    
    if (_isValid) {
        [_statusLabel setTextColor:[UIColor greenColor]];
        [_statusLabel setText:@"OK"];
    }
    
    NSString *newphone = [phoneUtil format:myNumber numberFormat:NBEPhoneNumberFormatINTERNATIONAL error:&aError];
    
    if (!aError) {
        [_numberLabel setText:newphone];
    }
    else{
        [_numberLabel setText:number];
    }
    
    validation = [phoneUtil isPossibleNumberWithReason:myNumber error:&aError];
    
    if (validation == NBEValidationResultTOO_SHORT) {
        [_statusLabel setTextColor:[UIColor redColor]];
        [_statusLabel setText:@"Too Short"];
    }
    
    if (validation == NBEValidationResultIS_POSSIBLE && !_isValid) {
        [_statusLabel setTextColor:[UIColor greenColor]];
        [_statusLabel setText:@"Ispossible"];
        
        NBPhoneNumber* temp = [phoneUtil parse:[NSString stringWithFormat:@"%@9", number] defaultRegion:countryCode error:&aError];
        
        validation = [phoneUtil isPossibleNumberWithReason:temp error:&aError];
        
        if (validation == NBEValidationResultTOO_LONG) {
            _isValid = YES;
            [_statusLabel setText:@"OK"];
            return;
        }
    }
    
}

#pragma mark - Handle button event

-(void)onPressingKeyPad:(UIButton*)sender{
    [sender setBackgroundColor:[UIColor grayColor]];
}

-(void)onReleasedKeyPad:(UIButton*)sender{
    [sender setBackgroundColor:[UIColor clearColor]];
}

-(void)onTapByKeyPad:(UIButton*)sender{
    switch (sender.tag) {
        case kcNumber:{
            
            if (!_isValid) {
                _numberString = [NSString stringWithFormat:@"%@%@", _numberString, sender.titleLabel.text];
                [self verifyPhoneNumber:_numberString withCode:_countryCode];
            }
            
        }
            break;
        case kcDelete:{
            if (_numberLabel.text.length > 0) {
                
                _isValid = NO;
                
                _numberString = [_numberString substringWithRange:NSMakeRange(0, _numberString.length-1)];
                [self verifyPhoneNumber:_numberString withCode:_countryCode];
            }
        }
            break;
        case kcCountry:{
            [_selectCountryView toggleInView:self.view];
        }
            break;
        default:
            break;
    }
    [sender setBackgroundColor:[UIColor clearColor]];
}

@end
