//
//  main.m
//  PhoneNumberValidate
//
//  Created by Minh Nguyen on 6/17/14.
//  Copyright (c) 2014 none. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PNVAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PNVAppDelegate class]));
    }
}
